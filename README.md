# Taskadd Bash

Check list for terminal. [.bashrc]

<><><><><><><><><><><><><><><><><><><><><><><><><><><><>

 put code bashtask in your .bashrc or .bash_aliases etc.

<><><><><><><><><><><><><><><><><><><><><><><><><><><><>




_example taskadd - asciinema -_
---
![bashtask](/uploads/2e9ccc830700d3ebd766f4529008103e/bashtask.png)


##### how to

- taskadd #Example: taskadd "Go grocery shopping"
- taskin  #Insert a task between items  : $ taskin 3 'hello world'
- taskrm  #Example: taskrm 2 --> Removes second item in list
- taskcl  #Delete and create a new taskfile

